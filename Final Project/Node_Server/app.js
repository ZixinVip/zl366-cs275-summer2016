var express = require('express');
var app = express();

var config = require('./config.json');

var mysql = require('mysql');
var con = mysql.createConnection({
	host:  config.Hostname,
	user:  config.Username, 	
	password: config.Password, 	
	database: config.database
});

var connect = require('connect');
var serveStatic = require('serve-static');

connect().use(serveStatic(__dirname)).listen(8080, function(){
    console.log('html Server Started on Port 8080');
});


con.connect(function(err){
	if (err){
		console.log("Error connecting to database"); 
	}
	else{
		console.log("Database successfully connected");  
	}
});


/*  //Backup For Showing HTML, doesn't load images though.
app.get('/index.html', function(req, res){
	res.sendFile(__dirname + '/index.html');
});
*/


app.get('/truckName', function (req, res) {//Get Whole Student Table
	res.header("Access-Control-Allow-Origin", "*");
	res.header("Access-Control-Allow-Headers", "X-Requested-With");
	
	con.query('SELECT FoodTruckName FROM foodtruck', function (err, rows, fields) {

		if (err){
			res.send(err);
		}
		else{
			res.send(rows);
		}

	});
  
});

app.get('/', function (req, res) {//Get Whole Student Table
	res.header("Access-Control-Allow-Origin", "*");
	res.header("Access-Control-Allow-Headers", "X-Requested-With");
	
	console.log(req.query);
	
	var sqlQuery = 'SELECT FoodName, Price, Image FROM menu3 WHERE TruckName="' + req.query.name + '"';
	
	console.log(sqlQuery);
	
	con.query(sqlQuery, function (err, rows, fields) {

		if (err){
			res.send(err);
		}
		else{
			res.send(rows);
		}

	});
  
	//console.log('req:',req.query);
  
});


app.get('/contact/', function (req, res) {//Get Whole Student Table
	res.header("Access-Control-Allow-Origin", "*");
	res.header("Access-Control-Allow-Headers", "X-Requested-With");
	
	console.log(req.query);
	
	var sqlQuery = 'SELECT Locations, Hours, Contact FROM foodtruck WHERE FoodTruckName="' + req.query.name + '"';
	
	console.log(sqlQuery);
	
	con.query(sqlQuery, function (err, rows, fields) {

		if (err){
			res.send(err);
		}
		else{
			res.send(rows);
		}

	});
  
	//console.log('req:',req.query);
  
});


app.listen(3000);
